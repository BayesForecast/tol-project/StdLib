//////////////////////////////////////////////////////////////////////////////
// ARCHIVO     : stat.tol
// DESCRIPCION : Funciones y variables de estad�stica cl�sica
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
// FUNCIONES
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
   Real StDsM(Set sample)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Card   (sample);
  SetStDs(sample)*Sqrt(n/(n-1))
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve la desviaci�n t�pica muestral o cuasi-desviaci�n t�pica, que "
  "se calcula como : \n"
  "\n"                      
  "   StDsM(X) = SetStDs(X)*Sqrt(n/(n-1)) \n"
  "\n" 
  "donde n = Card(X)\n"
  "La utilidad de este estad�stico se deriva de que es insesgado.", 
  StDsM);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
   Real VarM(Set sample)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Card   (sample);
  SetVar(sample)*(n/(n-1))
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve la varianza muestral o cuasi-varianza , que "
  "se calcula como : \n"
  "\n"                      
  "   VarM(X) = SetVar(X)*Sqrt(n/(n-1)) \n"
  "\n" 
  "donde n = Card(X)\n"
  "La utilidad de este estad�stico se deriva de que es insesgado.", 
  VarM);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
  Real SetRange(Set sample)
//////////////////////////////////////////////////////////////////////////////
{
  SetMax(sample)-SetMin(sample)
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve el rango o recorrido de una muestra, que "
  "se calcula como : \n"
  "\n"                      
  "   SetRange(X) = SetMax(X)-SetMin(X) \n" 
  "\nEjemplo : " 
  "\n"                      
  "   SetRange([[25,6,3,4,33,12,64,5,25,8,3,54,5,43,55,60]]) = 64-3 = 61\n", 
  SetRange);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
   Set SetMode(Set sample)
//////////////////////////////////////////////////////////////////////////////
{
  Set setMode = If(!Card(sample),Copy(Empty),
  {
    Set s1 = Classify(sample, Real(Real x1, Real x2) { Compare(x1,x2) } );
    Set s2 = EvalSet(s1, Set(Set c)
    {
      Real n = Card(c);
      Real x = c[1];
      [[n,x]]
    });
    Set s3 = Classify(s2,Real(Set y1, Set y2) { -Compare(y1[1],y2[1]) } );
    Set s4 = s3[1];
    Traspose(s4)[2]  
  })
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve la moda de un conjunto de datos, es decir, el conjunto de los "
  "valores m�s repetidos de la muestra. \n"
  "En primer lugar se clasifican los elementos de la muestra por su "
  "frecuencia y luego se toman los elementos de m�xima frecuencia que puede "
  "ser uno solo o varios que se repitan el mismo n�mero de veces. \n"
  "El resultado s�lo puede ser vac�o si la muestra es vac�a.\n"
  "Si todos los elementos de la muestra son distintos la moda es el propio "
  "conjunto de la muestra."
  "En el caso de haber varias modas los valores se devuelven ordenados de "
  "nemor a mayor." 
  "\nEjemplos : " 
  "\n"                      
  "   SetMode([[8,8,8,8,9,9,9,6,6,7]]) = [[8]]   \n"
  "   SetMode([[9,6,7,8,8,9,9,6,6,7]]) = [[6,9]] \n",
  SetMode);
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
// Funciones para test de hip�tesis m�s usuales
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
   Real PruebaChi(Matrix observado, Matrix esperado)
//////////////////////////////////////////////////////////////////////////////
{
  ro = Rows(observado);
  co = Columns(observado);
  re = Rows(esperado);
  ce = Columns(esperado);
  g  = (ro-1)*(co-1);
  If
  (
    Or(NE(ro,re),NE(co,ce)),
    ?,
    p =
    {
      i = 1;
      x = 0;
      While
      (
        i<=ro;
        w1 =
        {
          j = 1;
          While
          (
            j<=co;
            w2 =
            {
              y = MatDat(observado, i, j);
              z = MatDat(esperado,  i, j);
              t = ((y-z)^2)/z;
              x := x + t;
              j := j+1;
              x
            }
          );
          i := i+1;
          x
        }
      );
      1-DistChi(x,g)
    }
  )
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Esta prueba es un caso particular del contraste Chi-cuadrado "+
  "de Pearson. \n"+
  "Para cualquier distribuci�n de probabilidad, el sumatorio de  "+
  "( (O(i)-E(i) )^2) /E(i) , donde O(i) representa las frecuencias "+
  "observadas en cierto recinto y E(i) las frecuencias esperadas de "+
  "acuerdo con la hip�tesis nula, se comporta asint�ticamente como una "+
  "Chi-cuadrado del n�mero adecuado de grados de libertad."+
  "Cuando la estructura de datos tiene forma de tabla y las frecuencias "+
  "esperadas se construyen como (f(k)*c(m) )/ N donde :\n\n"+
  "  f(k) es el n�mero total de casos en la fila k-�sima, \n"+
  "  c(m) es el n�mero total de casos en la colomna m-�sima, y\n"+
  "  N    es el n�mero total de casos; \n\n"+
  "entonces el n�mero de grados de libertad es (f-1)*(c-1) donde 'f' es "+
  "el n�mero de filas de la tabla y 'c' el correspondiente n�mero de "+
  "columnas. Dadas las matrices de frecuencias observadas y esperadas, "+
  "nuestra funci�n PruebaChi devuelve la probabilidad de que una "+
  "Chi-cuadrado con (f-1)*(c-1) grados de libertad tome un valor mayor "+
  "o igual al obtenido al estad�stico descrito arriba.\n"+
  "Para que la aplicaci�n de la prueba sea v�lida es necesario que "+
  "las frecuencias esperadas no dependan de una estimaci�n de la "+
  "distribuci�n de probabilidad correspondiente realizada con los "+
  "mismos datos muestrales.",
  PruebaChi);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
   Real PruebaF(Set x1, Set x2)
//////////////////////////////////////////////////////////////////////////////
{
  Real n1 = Card(x1);
  Real n2 = Card(x2);
  Real s1 = VarM(x1);//*n1/(n1-1);
  Real s2 = VarM(x2);//*n2/(n2-1);
  Real F  = s1/s2;
  Real pf =  DistF(F,n1-1,n2-1);
  Min(2*pf,2*(1-pf)) 
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Dadas dos muestras, la funci�n PruebaF devuelve la probabilidad de que "+
  "las varianzas respectivas sean diferentes.",
  PruebaF);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
   Real TransFisherInv(Real x)
//////////////////////////////////////////////////////////////////////////////
{
  TanH(x)
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve la funci�n inversa de la transformaci�n de Fisher."+
  "Coincide con la tangente hiperb�lica.",
  TransFisherInv);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
   Real TransFisher(Real x)
//////////////////////////////////////////////////////////////////////////////
{
  ATanH(x)
};
//////////////////////////////////////////////////////////////////////////////
  PutDescription(
  "Devuelve la transformaci�n Fisher o coeficiente Z."+
  "Coincide con la inversa de la tangente hiperb�lica.",
  TransFisher);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Tablas de contingencia
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set ContingencyTable(Matrix Y, Matrix YEst, Real k)
//////////////////////////////////////////////////////////////////////////////
{
  Real numRows = Rows(Y); 
  Matrix A     = GE(YEst, Rand(numRows, 1, k, k));
  Matrix Zero  = SetCol(NCopy(numRows, 0));
  Matrix Unit  = SetCol(NCopy(numRows, 1));


  Matrix YZero = Eq(Y, Zero);
  Matrix YUnit = Eq(Y, Unit);
  
  Matrix AZero = Eq(A, Zero);
  Matrix AUnit = Eq(A, Unit);

  Matrix A00   =  And(YZero, AZero);    
  Matrix A01   =  And(YZero, AUnit);    
  Matrix A10   =  And(YUnit, AZero);    
  Matrix A11   =  And(YUnit, AUnit);    
  
  Real a00     = MatSum(A00);
  Real a01     = MatSum(A01);
  Real a10     = MatSum(A10);
  Real a11     = MatSum(A11);

  Set ContingencyEst = SetOfSet
  (
    SetOfText("","Est_0", "Est_1"),
    SetOfText("Real_0")<<SetOfReal(a00/(a00+a10), a01/(a11+a01)), 
    SetOfText("Real_1")<<SetOfReal(a10/(a00+a10), a11/(a11+a01))
  );
  Set ContingencyRea = SetOfSet
  (
    SetOfText("","Est_0", "Est_1"),
    SetOfText("Rea_0")<<SetOfReal(a00/(a00+a01), a01/(a00+a01)), 
    SetOfText("Rea_1")<<SetOfReal(a10/(a11+a10), a11/(a11+a10))
  );
  Set Numbers = SetOfSet
  (
    SetOfText("","Est_0", "Est_1"),
    SetOfText("Rea_0")<<SetOfReal(a00, a01), 
    SetOfText("Rea_1")<<SetOfReal(a10, a11)
  );

  SetOfAnything(ContingencyEst, ContingencyRea, Numbers, A)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("Return the precision and completitude contengency tables,
the number of cases of each type and the right cases vector for a cut point.", 
"Retorna las tablas de contingencia de precisi�n y completitud,
 el n�mero de casos de cada tipo y el vector de aciertos seg�n un punto de 
corte."), ContingencyTable);
//////////////////////////////////////////////////////////////////////////////

