//////////////////////////////////////////////////////////////////////////////
// FILE   : _arma_process.tol
// PURPOSE: Methods related with ARMA process
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock ARMAProcess = 
//////////////////////////////////////////////////////////////////////////////
[[
  Text _.autodoc.description = "Methods related with ARMA process";
  
  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.changeZeroCoefficient =
  "Change zero coefficients of a polynomial by a given value";
  Polyn changeZeroCoefficient(Polyn pol, Real nonZeroValue)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix M = PolMat(pol, Degree(pol)+1, 1);
    Matrix M_ = M + Not(M)*nonZeroValue;
    MatPol(Tra(M_))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.ACOV =
  "Returns theoretical autocovariance function from 0 to given order";
  Matrix ACOV(Polyn ar, Polyn ma, Real order)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix acov = ARMATACov(ar,ma,order+1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.ACF =
  "Returns theoretical autocorrelation function from 0 to given order";
  Matrix ACF(Polyn ar, Polyn ma, Real order)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix acov = ARMATACov(ar,ma,order+1);
    Matrix acf = acov * 1/MatDat(acov,1,1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.PACF =
  "Returns theoretical partial autocorrelation function from 0 to given order";
  Matrix PACF(Polyn ar, Polyn ma, Real order)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix acov = ARMATACov(ar,ma,order+1);
    Matrix acf = acov * 1/MatDat(acov,1,1);
    Matrix fi = DurbinAutoReg(Sub(acf,2,1,order,1),order);
    Matrix pacf = Col(1) << Tra(SubDiag(fi,0))
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.StationarityBounds.2.1 =
  "Returns stationary bounds for fi1 in a polynomial 1 - fi1*B - fi2*B^2";
  Set StationarityBounds.2.1(Polyn pol, Real periodicity)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real fi2 = -Coef(pol, 2*periodicity);
    SetOfReal(fi2-1,1-fi2)
  };
  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.StationarityBounds.2.2 =
  "Returns stationary bounds for fi2 in a polynomial 1 - fi1*B - fi2*B^2";
  Set StationarityBounds.2.2(Polyn pol, Real periodicity)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real fi1 = -Coef(pol, periodicity);
    Real max = If(fi1<=0, 1+fi1, 1-fi1);
    SetOfReal(-1,max)
  };
  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.StationarityBounds.2 =
  "Returns stationary bounds for the coefficient of degree 1 or 2 of a "
  "polynomial of degree 1 or 2";
  Set StationarityBounds.2(Polyn pol, Real periodicity, Real degree)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE StationarityBounds.2("<<pol+","<<periodicity+","<<degree+")");
    Real n = Degree(pol)/periodicity;
  //WriteLn("TRACE StationarityBounds.2 n="<<n);
    If(n==1, SetOfReal(-1,1),
    {
      Real d = degree/periodicity;
    //WriteLn("TRACE StationarityBounds.2 d="<<d);
      Case
      (
        And(n<=2,d==1), ARMAProcess::StationarityBounds.2.1(pol, periodicity),
        And(n<=2,d==2), ARMAProcess::StationarityBounds.2.2(pol, periodicity),
        n>=3, 
        {
          Real b = n-d+1;
        //WriteLn("TRACE StationarityBounds.2 b="<<b);
          SetOfReal(-b*periodicity,b*periodicity)
        }
      )
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix CholeskiFactorCov(VMatrix cov)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE CholeskiFactorCov("+FullName(cov)+") VRows(cov)="<<VRows(cov)+" Sparsity(cov)="<<Real(VNonNullCells(cov)/VRows(cov)^2));
    VMatrix c = Drop(cov,1.E-16*VRows(cov)^2);
    VMatrix L = CholeskiFactor(c,"X",True,False);
    If(VRows(L)==VRows(cov),
    {
    //WriteLn("TRACE CholeskiFactorCov L:"<<L);
      VMatrix pL = Pack(L,1);
    //WriteLn("TRACE CholeskiFactorCov pL:"<<pL);
      pL
    },
    {
      Warning("[StdLib::ARMAProcess::CholeskiFactorCov] No es posible "
       "aplicar CholeskiFactor a una matriz virtual no definida positiva");
      L
    })
  };

  ////////////////////////////////////////////////////////////////////////////
    VMatrix AvoidSymRoundErr(VMatrix S)
  ////////////////////////////////////////////////////////////////////////////
  {
    (S+Tra(S))*0.5
  };

  ////////////////////////////////////////////////////////////////////////////
    VMatrix ExtendBandedSymToeplitz(VMatrix S, Real m)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE ExtendBandedSymToeplitz Begin "+Time); 
    Real n = VRows(S);
    VMatrix s = ARMAProcess::AvoidSymRoundErr(Case(
      m==n, S,
      m<n,  Sub(S,1,1,m,m),
      m>n,
      {
        Polyn pol = 
        {
          Polyn aux = MatPol(Reverse(VMat2Mat(SubRow(S,[[n]]))));
          aux+ChangeBF(aux)-VMatDat(S,n,n)
        };
        VMatrix aux = (
          ((S - Pol2VMat(pol, n, n)) | Zeros(n, m-n)) <<
          Zeros(m-n,m)
        )+Pol2VMat(pol, m, m);
        aux
      }));
    Drop(s,1.E-16*n^2)
  }; 

  ////////////////////////////////////////////////////////////////////////////
    VMatrix ExtendBandedLowTriangToeplitz(VMatrix L, Real m)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE ExtendBandedLowTriangToeplitz Begin "+Time); 
    Real n = VRows(L);
    VMatrix L_ = Case(
      m==n, L,
      m<n,  Sub(L,1,1,m,m),
      m>n,
      {
        Polyn pol = MatPol(Reverse(VMat2Mat(SubRow(L,[[n]]))));
        VMatrix aux = (
          ((L - Pol2VMat(pol, n, n)) | Zeros(n, m-n)) <<
          Zeros(m-n,m)
        )+Pol2VMat(pol, m, m);
        aux
      });
    Drop(L_,1.E-16*n^2)
  }; 

  ////////////////////////////////////////////////////////////////////////////
  NameBlock FastCholeskiCovFactor(Polyn ar_, Polyn ma_, Real m_)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE FastCholeskiCovFactor Begin "+Time); 
    [[
    Text _.autodoc.description = 
    "Computes a Fast Choleski decomposition of the covariance S=L*L' of an "
    "ARMA process as a product of two virtual sparse banded matrices Li_ar "
    "and L_ma matching \n"
    "  L^-1 = L_ma ^-1 * Li_ar \n"
    "  L    = Li_ar^-1 * L_ma  \n"
    "Factorization is not explicitly built but just sparse factors are "
    "stored in O(n*m) cells, instead of O(m^2) using directly Cholesky over "
    "full covarianze matrix, being m the number of data and n=Max(p,q)+1+p, "
    "where p and q are degrees of AR and MA polynomials. Usually m is much "
    "greater than n, so a lot of CPU and RAM will be saved.\n"
    "Factoring complexity is O(n^2*m) instead of O(m^3) over full covariance"
    ".\n"
    "Method filter makes products of type L^-1 * X applying internally "
    "virtual matrix operators, where X can be a VMatrix, Matrix or Serie.\n"
    "Data length of the serie or VMatrix or Matrix rows must be just m."
    "If X is a VMatrix or a Matrix could have any number of columns and "
    "filter will apply in every one. Filtering complexity requieres also "
    "O(n*m) operations.\n"
    "The logarithm of the determinant of full covariance is stored in "
    "member _.logDetCov\n"
    "Example:\n"
    "NameBlock myFastChol = ARMAProcess::FastCholeskiCovFactor(ar,ma,m);\n"
    "...\n"
    "VMatrix Z1 = ...;\n"
    "VMatrix E1 = myFastChol::filter(Z1);\n"
    "...\n"
    "Matrix Z2 = ...;\n"
    "Matrix E2 = myFastChol::filter(Z2);\n"
    "...\n"
    "Serie Z3 = ...;\n"
    "Serie E3 = myFastChol::filter(Z3);\n";
    //Stores arguments as members
    Polyn _.ar = ar_;
  //Real _trc.1 = { WriteLn("TRACE FastCholeskiCovFactor _.ar=" << _.ar); 0 };
    Polyn _.ma = ma_;
  //Real _trc.2 = { WriteLn("TRACE FastCholeskiCovFactor _.ma=" << _.ma); 0 };
    Real _.m  = m_;
  //Real _trc.3 = { WriteLn("TRACE FastCholeskiCovFactor _.m=" << _.m); 0 };
    //Stores degrees of AR and MA polynomials
    Real _.p = Degree(_.ar);
  //Real _trc.4 = { WriteLn("TRACE FastCholeskiCovFactor _.p=" << _.p); 0 };
    Real _.q = Degree(_.ma);
  //Real _trc.5 = { WriteLn("TRACE FastCholeskiCovFactor _.q=" << _.q); 0 };
    //Calculates de minimum length of non Toeplitz main minor of factors
    Real _.n = Max(_.p,_.q)+_.p+1;
  //Real _trc.6 = { WriteLn("TRACE FastCholeskiCovFactor _.n=" << _.n); 0 };
    Real drop = 1.E-16 / _.n^2;
    //Calculates de main minor of length _.n of just pure AR covariance
    VMatrix _.cov_ar_n = 
      Drop(Pol2VMat(ARMATACov(_.ar,1,_.n), _.n, _.n),drop);
    //Does Choleski factorization of pure AR
  //Real _trc.7 = { WriteLn("TRACE FastCholeskiCovFactor _.cov_ar_n=" << _.cov_ar_n); 0 };
    VMatrix _.L_ar_n = ARMAProcess::CholeskiFactorCov(_.cov_ar_n);
  //Real _trc.8 = { WriteLn("TRACE FastCholeskiCovFactor _.L_ar_n=" << _.L_ar_n); 0 };
    //Calculates the inverse of Choleski factorization of pure AR
    VMatrix _.Li_ar_n = Drop(CholeskiSolve(_.L_ar_n,Eye(_.n),"PtL"),drop);
  //Real _trc.9 = { WriteLn("TRACE FastCholeskiCovFactor _.Li_ar_n=" << _.Li_ar_n); 0 };
    //Extends main minor of length _.n to full data length _.m as Toplitz from  
    //last row. Terms _.n^2 is added and substracted to clean rounding error.
    VMatrix _.Li_ar= Drop(
     ARMAProcess::ExtendBandedLowTriangToeplitz(_.Li_ar_n,_.m),drop);
  //Real _trc.10 = { WriteLn("TRACE FastCholeskiCovFactor _.Li_ar=" << _.Li_ar); 0 };

    //Calculates de main minor of length _.n of joint ARMA covariance
    VMatrix _.cov_arma_n = 
      Drop(Pol2VMat(ARMATACov(_.ar,_.ma,_.n), _.n, _.n),drop);
    //Filters full ARMA covariance of AR factor 
    VMatrix _.cov_ma_n = Drop(_.Li_ar_n*_.cov_arma_n*Tra(_.Li_ar_n),drop);
    //Extends main minor of length _.n to full data length _.m as Toplitz from 
    //last row and column getting a symmetric and sparse banded matrix
  //Real _trc.11 = { WriteLn("TRACE FastCholeskiCovFactor _.cov_ma_n=" << _.cov_ma_n); 0 };
    VMatrix _.cov_ma_m = Drop(
      ARMAProcess::ExtendBandedSymToeplitz(_.cov_ma_n,_.m),drop);
  //Real _trc.12 = { WriteLn("TRACE FastCholeskiCovFactor _.cov_ma_m=" << _.cov_ma_m); 0 };
    //Does Choleski factorization of pseudo MA factor
    VMatrix _.L_ma = ARMAProcess::CholeskiFactorCov(_.cov_ma_m);
    //Converts to Cholmod.R.Sparse
    VMatrix _.L_ma_sp = Drop(Convert(_.L_ma,"Cholmod.R.Sparse"),drop);
    //Calculates de logarithm of determinant of _.m ARMA covarianze 
    Real _.logDetCov = 2*(-VMatSum(Log(SubDiag(_.Li_ar,0)))+
                           VMatSum(Log(SubDiag(_.L_ma_sp, 0))));
    //Filters a Matrix, VMatrix or Serie from inverse of Choleski factor of
    //_.m ARMA covarianze 
    Anything filter(Anything X)
    {
      Text g = Grammar(X);
      Case
      (
        g=="VMatrix",
        {
          CholeskiSolve(_.L_ma,_.Li_ar*X,"PtL")
        },
        g=="Matrix",
        {
          VMatrix aux1 = Mat2VMat(X);
          VMatrix aux2 = CholeskiSolve(_.L_ma,_.Li_ar*aux1,"PtL");
          VMat2Mat(aux2)
        },
        g=="Serie",
        {
          Matrix  aux0 = Tra(SerSetMat([[X]]));
          VMatrix aux1 = Mat2VMat(X);
          VMatrix aux2 = CholeskiSolve(_.L_ma,_.Li_ar*aux1,"PtL");
          Matrix  aux3 = VMat2Mat(aux2);
          MatSerSet(Tra(aux3),Dating(X),First(X))[1]
        },
        1==1,
        {
          WriteLn("Cannot apply ARMACovFastCholeskiFactor::filter to type "+g)
        }
      )
    }
  ]]};

  //////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.Eval.Almagro =
  "Given an ARMA process ar(B)*z[t] = ma(B)*a[t] builds these methods:\n"
  " Draw.U_cond_Z: generate random initial values conditioned to noise\n"
  " LogLH.Z_cond_U: conditional likelihood of noise conditioned to given "
                    "initial values\n.";
  NameBlock Eval.Almagro(Polyn ar, Polyn ma, VMatrix z_, Real sigma)
  //////////////////////////////////////////////////////////////////////////////
  {
    Real    m     = VRows(z_);
    Real    p     = Degree(ar);
    Real    q     = Degree(ma);
    Real drop = 1.E-16/m^2;
  //WriteLn("TRACE Eval.Almagro Begin "+Time); 
  //WriteLn("TRACE Eval.Almagro(p"<<p+",q="<<q+",m="<<m);
  //Polyn   _.ar    = ARMAProcess::changeZeroCoefficient(ar,1E-5);
  //Polyn   _.ma    = ARMAProcess::changeZeroCoefficient(ma,1E-5);
    Polyn   _.ar    = ar;
    Polyn   _.ma    = ma;

    NameBlock neutral = [[
    Polyn   _.ar;
    Polyn   _.ma;
    VMatrix _.z     = z_;
    VMatrix _.z0    = Rand(0,0,0,0);
    VMatrix _.a0    = Rand(0,0,0,0);
    Real    _.sigma = sigma;
    Real    _.s2    = _.sigma*_.sigma;
    Real    _.m     = m;
    Real    _.p     = p;
    Real    _.q     = q;
    Real    _.q_p   = _.q+_.p;
    Real    _.mxpq  = Max(_.p,_.q);
    VMatrix _.a     = z_;
    Real _.det_cov_z = 0;
    Real _.det_cov_u = 0;
    Real _.det_cov_G = 0;
    Real _0.5_m_log2_PI_s2 = -0.5*_.m*Log(2*PI*_.s2);
    Real _.logLH = _0.5_m_log2_PI_s2 - 0.5*_.m* VMatMoment(_.a,2)/_.s2
    ]];
    NameBlock nonStationary = [[
    ////////////////////////////////////////////////////////////////////////////
    Real LogLH.Z(VMatrix z)
    ////////////////////////////////////////////////////////////////////////////
    {
      VMatrix _.z := z;
      VMatrix _.pi_z := DifEq(_.ar/_.ma,_.z);
      VMatrix _.G_pi_z := Tra(Tra(_.pi_z)*_.G);
    //Real _trc.8 = { WriteLn("TRACE Eval.Almagro Creating various "+Time); 0 };
      VMatrix _.u := CholeskiSolve(_.L_G, _.G_pi_z,"PtLLtP");
      VMatrix _.z0 := Get.Z0(_.u);
      VMatrix _.a0 := Get.A0(_.u);
      VMatrix _.v := CholeskiSolve(_.L_u, _.u,"PtL");
      VMatrix _.a := DifEq(_.ar/_.ma, _.z, _.z0, _.a0);

      Real aa = MatDat(VMat2Mat( Tra(_.a)*_.a), 1,1);     
      Real uu = MatDat(VMat2Mat( Tra(_.u)*_.covi_u*_.u),1,1);
      Real _.logLH := _0.5_m_log2_PI_s2 - 0.5*(_.det_cov_z+(aa+uu)/_.s2);
      _.logLH
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Draw.U_cond_Z(Real unused)
    ////////////////////////////////////////////////////////////////////////////
    {
      VMatrix u01 = Gaussian(_.q_p,1,0,_.sigma);
      _.u + CholeskiSolve(_.L_G, u01,"LtP")
    };
    ////////////////////////////////////////////////////////////////////////////
    Real LogLH.Z_cond_U(Polyn ar, Polyn ma, VMatrix z0, VMatrix a0)
    ////////////////////////////////////////////////////////////////////////////
    {
      -_highValue
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Get.Z0(VMatrix u)
    ////////////////////////////////////////////////////////////////////////////
    {
      Sub(u, 1, 1, _.p, 1)
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Get.A0(VMatrix u)
    ////////////////////////////////////////////////////////////////////////////
    {
      Sub(u, _.p+1, 1, _.q, 1)
    };
    Real _highValue = 10000*sigma;
    Polyn   _.ar;
    Polyn   _.ma; 
    VMatrix _.z     = z_;
    VMatrix _.z0    = Rand(0,0,_highValue,_highValue);
    VMatrix _.a0    = Rand(0,0,_highValue,_highValue);
    Real    _.sigma = sigma;
    Real    _.s2    = _.sigma*_.sigma;
    Real    _.m     = m;
    Real    _.p     = p;
    Real    _.q     = q;
    Real    _.q_p   = _.q+_.p;
    Real    _.mxpq  = Max(_.p,_.q);
    VMatrix _.a     = z_*_highValue;
    Real _.det_cov_z = _highValue;
    Real _.det_cov_u = _highValue;
    Real _.det_cov_G = _highValue;
    Real _.logLH = _highValue
    ]];

    If(!p & !q, neutral, {

    Polyn   _.ar;
    Polyn   _.ma; 
    VMatrix _.z     = z_;
    Real    _.sigma = sigma;
    Real    _.s2    = _.sigma*_.sigma;
    Real    _.m     = m;
    Real    _.p     = p;
    Real    _.q     = q;
    Real    _.q_p   = _.q+_.p;
    Real    _.mxpq  = Max(_.p,_.q);


  //Real _trc.1 = { WriteLn("TRACE Eval.Almagro Creating _.cov_u "+Time); 0 };
    VMatrix _.cov_u = Case(
    _.p & _.q,
    {
      VMatrix cov_zz = Drop(Pol2VMat(ARMATACov(_.ar,_.ma,_.p), _.p, _.p),drop);
    //WriteLn("TRACE _.cov_zz = "<<_.cov_zz);
      VMatrix cov_aa = Eye(_.q);

      Polyn psi = Expand(_.ma/_.ar,Max(_.p,_.q));

      Polyn psi_F = ChangeBF(psi)*B^Max(0,_.q-_.p);
      VMatrix cov_az = If(_.q>=_.p, Pol2VMat(psi_F, _.q, _.p), 
                            Zeros(_.q, _.p-_.q) | Pol2VMat(psi_F, _.q, _.q));
      VMatrix cov_za = Tra(cov_az);
/*
      VMatrix cov_za_ = Mat2VMat(ForMat(_.p,_.q,Real(Real a, Real b)
      {
        Real k = _.p + 1 - a;
        Real i = _.q + 1 - b;
        Coef(psi,i-k)
      }));
     VMatrix cov_za_dif = cov_za-cov_za_;
     Real cov_za.ok = 1-VMatMax(Abs(cov_za_dif));
*/
    //WriteLn("TRACE _.cov_za = "<<_.cov_za);

      (cov_zz | cov_za ) <<
      (cov_az | cov_aa )
    },
    _.p & !_.q,
    {
      Drop(Pol2VMat(ARMATACov(_.ar,_.ma,_.p), _.p, _.p),drop)
    },
    !_.p & _.q,
    {
      Eye(_.q)
    });

  //Real _trc.2 = { WriteLn("TRACE Eval.Almagro Creating _.L_u "+Time); 0 };
  //WriteLn("_.cov_u = "<<_.cov_u);
    VMatrix _.L_u  = ARMAProcess::CholeskiFactorCov(_.cov_u);
    If(!VRows(_.L_u), {
    WriteLn("[ARMAProcess::Eval.Almagro] Cannot create _.L_u with \n"+
      "AR="<<_.ar+"\n"+
      "MA="<<_.ma+"\n");
    nonStationary
    },{
  //Real _trc.3 = { WriteLn("TRACE Eval.Almagro Creating _.covi_u "+Time); 0 };
    VMatrix _.covi_u = 
    {
      aux = CholeskiSolve(_.L_u,Eye(_.q_p),"PtLLtP");
      Drop(ARMAProcess::AvoidSymRoundErr(aux),drop)
    };
  //Real _trc.4 = { WriteLn("TRACE Eval.Almagro Creating _.L_u_sp "+Time); 0 };
    VMatrix _.L_u_sp = Drop(Convert(_.L_u,"Cholmod.R.Sparse"),drop);
    Real _.det_cov_u = 2*VMatSum(Log(SubDiag(_.L_u_sp,0)));
  //Real _trc.5 = { WriteLn("TRACE Eval.Almagro Creating H "+Time); 0 };
    VMatrix _.H1 = Drop(Pol2VMat(F^_.p*(1-_.ar), _.m, _.p),drop);
  //Real _trc.5.0.1 = { WriteLn("TRACE Eval.Almagro _.H1 "<<_.H1); 0 };
    VMatrix _.H2 = Drop(Pol2VMat(F^_.q*(_.ma-1), _.m, _.q),drop);
  //Real _trc.5.0.2 = { WriteLn("TRACE Eval.Almagro _.H2 "<<_.H2); 0 };
    VMatrix _.H = _.H1 | _.H2;
  //Real _trc.5.0.3 = { WriteLn("TRACE Eval.Almagro _.H "<<_.H); 0 };
  //Real _trc.5.0 = { WriteLn("TRACE Eval.Almagro Creating G "+Time); 0 };
    VMatrix _.G = Drop(Convert(
      DifEq(1/_.ma, _.H, Rand(0,_.q_p,0,0), Rand(_.q,_.q_p,0,0) ),"Cholmod.R.Sparse"),drop);
  //Real _trc.5.0.4 = { WriteLn("TRACE Eval.Almagro _.G "<<_.G); 0 };
    VMatrix _.GtG = MtMSqr(_.G);
  //Real _trc.5.1 = { WriteLn("TRACE Eval.Almagro Creating cov_G "+Time); 0 };
  //Real _trc.5.2 = { WriteLn("TRACE Eval.Almagro _.covi_u "<<_.covi_u); 0 };
  //Real _trc.5.3 = { WriteLn("TRACE Eval.Almagro _.GtG "<<_.GtG); 0 };
    VMatrix _.cov_G = _.covi_u + _.GtG;
  //Real _trc.6 = { WriteLn("TRACE Eval.Almagro Creating _.L_G "+Time); 0 };
    VMatrix _.L_G   = ARMAProcess::CholeskiFactorCov(_.cov_G);

    If(!VRows(_.L_G), {
    WriteLn("[ARMAProcess::Eval.Almagro] Cannot create _.L_G with \n"+
      "AR="<<_.ar+"\n"+
      "MA="<<_.ma+"\n");
    nonStationary
    },{ [[

    ////////////////////////////////////////////////////////////////////////////
    Real LogLH.Z(VMatrix z)
    ////////////////////////////////////////////////////////////////////////////
    {
      VMatrix _.z := z;
      VMatrix _.pi_z := DifEq(_.ar/_.ma,_.z);
      VMatrix _.G_pi_z := Tra(Tra(_.pi_z)*_.G);
    //Real _trc.8 = { WriteLn("TRACE Eval.Almagro Creating various "+Time); 0 };
      VMatrix _.u := CholeskiSolve(_.L_G, _.G_pi_z,"PtLLtP");
      VMatrix _.z0 := Get.Z0(_.u);
      VMatrix _.a0 := Get.A0(_.u);
      VMatrix _.v := CholeskiSolve(_.L_u, _.u,"PtL");
      VMatrix _.a := DifEq(_.ar/_.ma, _.z, _.z0, _.a0);

      Real aa = MatDat(VMat2Mat( Tra(_.a)*_.a), 1,1);     
      Real uu = MatDat(VMat2Mat( Tra(_.u)*_.covi_u*_.u),1,1);
      Real _.logLH := _0.5_m_log2_PI_s2 - 0.5*(_.det_cov_z+(aa+uu)/_.s2);
      _.logLH
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Draw.U_cond_Z(Real unused)
    ////////////////////////////////////////////////////////////////////////////
    {
      VMatrix u01 = Gaussian(_.q_p,1,0,_.sigma);
      _.u + CholeskiSolve(_.L_G, u01,"LtP")
    };

    ////////////////////////////////////////////////////////////////////////////
    Real LogLH.Z_cond_U(Polyn ar, Polyn ma, VMatrix z0, VMatrix a0)
    ////////////////////////////////////////////////////////////////////////////
    {
      VMatrix a = DifEq(ar/ma,_.z, z0, a0);
      _0.5_m_log2_PI_s2 - 0.5*_.m*VMatMoment(a,2)/_.s2
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Get.Z0(VMatrix u)
    ////////////////////////////////////////////////////////////////////////////
    {
      Sub(u, 1, 1, _.p, 1)
    };
    ////////////////////////////////////////////////////////////////////////////
    VMatrix Get.A0(VMatrix u)
    ////////////////////////////////////////////////////////////////////////////
    {
      Sub(u, _.p+1, 1, _.q, 1)
    };

    Polyn   _.ar;
    Polyn   _.ma; 
    VMatrix _.z;
    Real    _.sigma;
    Real    _.s2;
    Real    _.m;
    Real    _.p;
    Real    _.q;
    Real    _.q_p;
    Real    _.mxpq;
    VMatrix _.cov_u;
    VMatrix _.L_u;
    VMatrix _.covi_u;
    VMatrix _.L_u_sp;
    Real _.det_cov_u;
    VMatrix _.H1;
    VMatrix _.H2;
    VMatrix _.H;
    VMatrix _.G;
    VMatrix _.GtG;
    VMatrix _.cov_G;
    VMatrix _.L_G;


  //Real _trc.7 = { WriteLn("TRACE Eval.Almagro Creating _.L_G_sp "+Time); 0 };
    VMatrix _.L_G_sp = Drop(Convert(_.L_G,"Cholmod.R.Sparse"),drop);
    Real _.det_cov_G = 2*VMatSum(Log(SubDiag(_.L_G_sp,0)));
    Real _.det_cov_z = _.det_cov_u + _.det_cov_G;
    Real _0.5_m_log2_PI_s2 = -0.5*_.m*Log(2*PI*_.s2);
    VMatrix _.pi_z = Rand(0,0,0,0);
    VMatrix _.G_pi_z = Rand(0,0,0,0);
    VMatrix _.u = Rand(0,0,0,0);
    VMatrix _.z0 = Rand(0,0,0,0);
    VMatrix _.a0 = Rand(0,0,0,0);
    VMatrix _.v = Rand(0,0,0,0);
    VMatrix _.a = Rand(0,0,0,0);

    VMatrix _R0100 = Rand(0,  1,0,0);
    VMatrix _Rq100 = Rand(_.q,1,0,0);

    Real _.logLH = ?;
  //Real _trc.9 = { WriteLn("TRACE Eval.Almagro End "+Time); 0 };
    Real _logLH = LogLH.Z(_.z)


  ]]}) }) }) }

]];


