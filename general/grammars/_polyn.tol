//////////////////////////////////////////////////////////////////////////////
// FILE    : _polyn.tol
// PURPOSE : Polyn added functions
// VERSION : 2003/09/03 CPA
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
  Polyn PsiWeights(Set arima, Real lag)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn ari = ARIMAGetARI(arima);
  Polyn ma  = ARIMAGetMA (arima);
  Expand(ma/ari,lag-1)
};  
  

//////////////////////////////////////////////////////////////////////////////
  Polyn PiWeights(Set arima, Real lag)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn ari = ARIMAGetARI(arima);
  Polyn ma  = ARIMAGetMA (arima);
  Expand(ari/ma,lag-1)
};  
  

//////////////////////////////////////////////////////////////////////////////
  Real PolCountCoef(Polyn p)
//
// PURPOSE: Devuelve el numero de coeficientes no nulos
//
//////////////////////////////////////////////////////////////////////////////
{
  Real gPol = Degree(p);
  Set Coeficientes = EvalSet(Range(0,gPol,1), Real(Real r){ Coef(p,r) });
  Set CoefNoNulos = Select(Coeficientes, Real(Real r) { Not(r==0) });
  Card(CoefNoNulos)
};


//////////////////////////////////////////////////////////////////////////////
   Set PolToSet(Polyn p)
//
// PURPOSE: Devuelve el conjunto de pares (coeficiente,exponente de B)
//
//////////////////////////////////////////////////////////////////////////////
{
  Real gPol = Degree(p);
  Set par = EvalSet(Range(0,gPol,1), Set(Real r) { [[Coef(p,r),r]] });
  Set setPol = Select(par, Real(Set s) { Real(s[1]) != 0 });
  If(Card(setPol)==0,par,setPol)
 
};


//////////////////////////////////////////////////////////////////////////////
   Text MonToText(Set par)
//
// PURPOSE: Devuelve el texto de un monomio de un polinomio.
//          Si Expo=0 -> Coef
//            Si Expo=1
//               Si Coef=1 -> B
//               Si Coef=-1 -> -B
//               En otro caso -> Coef*B
//            En otro caso
//               Si Coef=1 -> B^Expo
//               Si Coef=-1 -> -B^Expo
//               En otro caso -> Coef*B^Expo
//
//////////////////////////////////////////////////////////////////////////////
{
  Real expo=par[2]; // Exponente
  Real coef=par[1]; // Coeficiente

  If(EQ(expo,0),FormatReal(coef),
     If(EQ(expo,1),
        If(EQ(coef,1),"B",
           If(EQ(coef,-1),"-B",
              FormatReal(coef)+"*B"
             )
          ),
        If(EQ(coef,1),"B^"+FormatReal(expo),
           If(EQ(coef,-1),"-B^"+FormatReal(expo),
              FormatReal(coef)+"*B^"+FormatReal(expo)
             )
          )
       )
    )
};


//////////////////////////////////////////////////////////////////////////////
   Text PolToText(Polyn p)
//
// PURPOSE: Devuelve un conjunto de texto.
//
//////////////////////////////////////////////////////////////////////////////
{
  Set  pcg = PolToSet(p);
  Real gradoPrimero =If(Card(pcg) != 0, pcg[1][2],0 );
  Text putPlus(Set par)
  { If(Or(EQ(par[2],gradoPrimero), LT(par[1],0)),
       MonToText(par),
       "+" + MonToText(par)) };

  Set texPar = EvalSet(pcg, putPlus);
  Set texParInv = 
      EvalSet(Range(1,Card(pcg),1),Text(Real r)
                                          { texPar[Card(pcg)+1-r] } );
  SetSum(texParInv)
};

//////////////////////////////////////////////////////////////////////////////

//
// PURPOSE : Aplica un polinomio a una serie
//////////////////////////////////////////////////////////////////////////////

   Serie AplicaInput(Set i)
{
  Polyn p = Eval(i[1]);
  Set If(Not(ObjectExist("Serie",i[2])),
         View(SetOfText("AplicaInput(): "+i[2]+" no existe"),""));
  Serie s = Eval(i[2]);
  p:s
};


//////////////////////////////////////////////////////////////////////////////
   Polyn PolNotNeg(Polyn pol)
//
// PURPOSE: Devuelve el polinomio sin sus terminos con coeficientes negativos
//
//////////////////////////////////////////////////////////////////////////////
{
  Set coefExp = PolToSet(pol);
  Set coefNotNeg = EvalSet(coefExp,Set(Set s)
                   {
                     Real a = s[1];
                     Real b = s[2];
                     SetOfAnything(If(GT(Sign(a),0),a,0),Real b)
                   });
  Set polTerm = EvalSet(coefNotNeg,Polyn(Set s)
                {
                  Real a = s[1];
                  Real b = s[2];
                  Polyn p = Eval("a*B^b");
                  p
                });
  SetSum(polTerm)
};



//////////////////////////////////////////////////////////////////////////////
   Polyn TendPolNotPos(Polyn pol)
//
// PURPOSE: Es un caso particular de PolNotNeg.Se aplica a las tendencias. 
//          Devuelve el polinomio sin sus terminos con coeficientes positivos.
//
//////////////////////////////////////////////////////////////////////////////
{
  Set coefExp = PolToSet(pol);
  Set coefNotNeg = EvalSet(coefExp,Set(Set s)
                   {
                     Real a = s[1];
                     Real b = s[2];
                     SetOfAnything(If(LT(Sign(a),0),a,0),Real b)
                   });
  Set polTerm = EvalSet(coefNotNeg,Polyn(Set s)
                {
                  Real a = s[1];
                  Real b = s[2];
                  Polyn p = Eval("a*B^b");
                  p
                });
  SetSum(polTerm)
};



//////////////////////////////////////////////////////////////////////////////
   Polyn GetCoef(Set tabinp,Text input)
//
// PURPOSE : De un conjunto de inputs contenidos en una tabla,
//           devuelve el coeficiente del input introducido. Si no existe
//           devuelve 0.  
//////////////////////////////////////////////////////////////////////////////
{
  Set getSet    = Select(tabinp, Real(Set s){input==s->Var});
  If(Card(getSet),getSet[1]->Polinomial,0)
};


//////////////////////////////////////////////////////////////////////////////
   Serie DifEqP(Polyn pinput,Serie input,Polyn poutput,Serie output)
//
// PURPOSE: Aplica una ecuacion en diferencias sin restricciones respecto a la
//          longitud de las series, exceptuando la natural de que la serie
//          input debe ser mas larga que la serie ouput y que input y output
//          deben tener suficientes datos para poder aplicar los polinomios.
//////////////////////////////////////////////////////////////////////////////
{
 Serie out = SubSer(output,Succ(Last(output),Dating(output),
                                            -Degree(poutput)+1),
                           Last(output));
 
 Serie in  = SubSer(input,Succ(Last(output),Dating(output),
                                           -Degree(pinput)+1),
                          Last(input));

 Set View(SetOfDate(Succ(Last(output),Dating(output),
                                            -Degree(poutput)+1),
                    Succ(Last(output),Dating(output),
                                           -Degree(pinput)+1)),"");
 Ration io  = pinput/poutput;
 DifEq(io,in,out)
};


//////////////////////////////////////////////////////////////////////////////
Polyn GetMediumWeigth(Serie original)
//////////////////////////////////////////////////////////////////////////////
{
  Real total  = SumS(original);
  Set coefPol = For(1, 7, Real(Real wd)
  { SumS(CalInd(WD(wd), Diario)*original)/total });
  Polyn weigthPol = MatPol(GetNumeric(coefPol)); 
  weigthPol
}; 

//////////////////////////////////////////////////////////////////////////////
Polyn GetMediumWeigthAnuMen(Serie original)
//////////////////////////////////////////////////////////////////////////////
{
  Real total  = SumS(original);
  Set coefPol = For(1, 12, Real(Real m)
  { SumS(CalInd(M(m), Mensual)*original)/total });
  Polyn weigthPol = MatPol(GetNumeric(coefPol)); 
  weigthPol
}; 

//////////////////////////////////////////////////////////////////////////////
  Polyn GetPolyn(Set tabInp, Text inputTxt)
//////////////////////////////////////////////////////////////////////////////
{
  Set sSel = Select(tabInp, Real(Set s){ s->X == inputTxt });

  If(Not(IsEmpty(sSel)), sSel[1]->Omega,
  {
    WriteLn("GetPolyn:\nNo se encuenta el input "+ inputTxt +
                      " en la tabla "+ Name(tabInp));
    UnknownPolyn
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Devuelve el coeficiente de un input perteneciente a una tabla de inputs con
estructura InputWidthName. Si no existe el input devuelve UnknownPolyn.",
GetPolyn);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
  Polyn FiniteUnitPolyn(Real amort, Real grado)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn alfa = Expand(1/(1-amort*B), grado);
  Polyn aux  = (1-B)*alfa + (amort^grado)*B^(grado+1) - 1;
  Real div   = -EvalPol(aux,1);
  Polyn norm = (1/div)*aux;
  1 + norm
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Construye un polinomio finito con una raiz unitaria de grado n y tipo de 
amortiguamiento amort.",
FiniteUnitPolyn);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
  Polyn PolynFromParamInf(Set paramInf)
//////////////////////////////////////////////////////////////////////////////
{
  Real  value = paramInf->Value;
  Polyn pol   = B^(paramInf->Order);
  value*pol
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Devuelve el polinomio estimado a partir de un conjunto con estructura
ParamInf.",
PolynFromParamInf);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Polyn AvrPol(Set sPolyn)
//////////////////////////////////////////////////////////////////////////////
{
  Real  numPol  = Card(sPolyn);
  Polyn sumaPol = SetSum(sPolyn);
  sumaPol/numPol
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Devuelve el polinomio cuyos coeficientes son la media de los coeficientes
correspondientes a los polinomios del conjunto que se pasa como parametro. Se
consideran tambien los valores 0 de los coeficientes sin grado. Y el
 polinomio desconocido.",
AvrPol);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Polyn AvrPolSinOmit(Set sPolyn)
//////////////////////////////////////////////////////////////////////////////
{
  Set polNoOmit = Select(sPolyn, Real(Polyn pol) 
  { 
    Not(IsUnknownPolyn(pol)) 
  });
  Real numPolNoOmit  = Card(polNoOmit);
  Polyn sumaPol      = SetSum(polNoOmit);
  sumaPol/numPolNoOmit
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Devuelve el polinomio cuyos coeficientes son la media de los coeficientes
correspondientes a los polinomios del conjunto que se pasa como parametro.
No se tienen en cuenta los polinomios ? para el calculo.",
AvrPolSinOmit);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Polyn StDsPolSinOmit(Set sPolyn)
//////////////////////////////////////////////////////////////////////////////
{
  Set  polNoOmit    = Select(sPolyn, Real(Polyn pol) {Not(IsUnknownPolyn(pol))});
  Real numPolNoOmit = Card(polNoOmit);
  Set polNoOmit2    = EvalSet(polNoOmit, Polyn(Polyn pol){pol^2});
  Polyn sum2        = SetSum(polNoOmit2);
  sum2/numPolNoOmit - AvrPol(polNoOmit)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("
Devuelve el polinomio cuyos coeficientes son la desviacion tipica de los
coeficientes correspondientes a los polinomios del conjunto que se pasa como
parametro. No se tienen en cuenta los polinomios omitidos",
StDsPolSinOmit);
//////////////////////////////////////////////////////////////////////////////

