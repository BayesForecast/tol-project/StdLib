# Read a single line of input from the terminal without echoing to the
# screen.  If Control-C is pressed, exit immediately.
#
proc tty_gets_no_echo {args} {
  array set option {
    -prompt ""
    -title ""
  }
  array set option $args
  if {$option(-title) ne ""} {
    puts $option(-title)
  }
  if {$option(-prompt) ne ""} {
    puts -nonewline $option(-prompt)
  }
  flush stdout
  global _tty_input _tty_wait tcl_platform
  if {$tcl_platform(platform) ne "unix"} {
    # FIXME:  This routine only works on unix.  On other platforms, the
    # password is still echoed to the screen as it is typed.
    return [gets stdin]
  }
  set _tty_input {}
  set _tty_wait 0
  fileevent stdin readable _tty_read_one_character
  exec /bin/stty raw -echo <@stdin
  vwait ::_tty_wait
  fileevent stdin readable {}
  return $_tty_input
}

proc _tty_read_one_character {} {
  set c [read stdin 1]
  if {$c=="\n" || $c=="\003"} {
    exec /bin/stty -raw echo <@stdin
    puts ""
    if {$c=="\003"} exit
    incr ::_tty_wait
  } else {
    append ::_tty_input $c
  }
}

proc twapi_gets_no_echo {args} {
  array set option {
    -prompt ""
    -title ""
  }
  array set option $args
  if {$option(-title) ne ""} {
    puts $option(-title)
  }
  if {$option(-prompt) ne ""} {
    puts -nonewline $option(-prompt)
  }
  flush stdout
  set oldmode [twapi::modify_console_input_mode stdin -echoinput false -lineinput true]
  gets stdin password
  # Restore original input mode
  eval [list twapi::set_console_input_mode stdin] $oldmode
}

proc tk_gets_no_echo {args} {
  array set option {
    -prompt ""
    -parent .
    -title "Input string"
  }
  array set option $args
  if {$option(-parent) eq "."} {
    set _w .inputKey
  } else {
    set _w $option(-parent).inputKey
  }
  set i 0
  while {[winfo exist ${_w}${i}]} {
    incr i
  } 
  set w ${_w}${i}
  toplevel $w -borderwidth 10
  wm title $w [msgcat::mc $option(-title)]

  # change the following to 'wm resizable $w 0 0' to prevent resizing
  wm resizable $w 1 0
  wm protocol $w WM_DELETE_WINDOW {set _inputExit 0}
  if {$option(-prompt) ne ""} {
    label  $w.p -text [msgcat::mc $option(-prompt)]
  }
  entry  $w.pass -show * -textvar _password
  button $w.ok -text OK -width 8 -command {set _inputExit 1}
  button $w.cancel -text Cancel -width 8 -command {set _inputExit 0}
  if {$option(-prompt) ne ""} {
    grid $w.p $w.pass - - -sticky new
  } else {
    grid $w.pass - - - -sticky new
  }
  grid rowconfigure $w 10 -minsize 20
  grid x x $w.ok $w.cancel -sticky e -row 11 -padx 2 -pady 2
  grid columnconfigure $w 1 -weight 1

  bind $w <Return> [list $w.ok invoke]
  bind $w <Escape> [list $w.cancel invoke]
  wm withdraw $w
  update idletasks
  set x [expr [winfo screenwidth $w]/2 - [winfo reqwidth $w]/2 \
             - [winfo vrootx [winfo parent $w]]]
  set y [expr [winfo screenheight $w]/2 - [winfo reqheight $w]/2 \
             - [winfo vrooty [winfo parent $w]]]
  wm geom $w +$x+$y
  wm deiconify $w
  raise $w
  grab set $w

  focus $w.pass

  vwait ::_inputExit
  if { $::_inputExit } {
    set key $::_password
  } else {
    set key ""
  }
  destroy $w
  unset ::_password
  return $key
}

if {[namespace exists tk]} {
  interp alias {} gets_no_echo {} tk_gets_no_echo
} elseif {$tcl_platform(platform) eq "windows"} {
  package require twapi
  interp alias {} gets_no_echo {} twapi_gets_no_echo
} else {
  interp alias {} gets_no_echo {} tty_gets_no_echo
}