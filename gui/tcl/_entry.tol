//////////////////////////////////////////////////////////////////////////////
// FILE   : _entry.tol
// PURPOSE: 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// INCLUDES
//////////////////////////////////////////////////////////////////////////////
//Set Include(PathIniSadd(0));

//////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// PROCEDURES
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclDialogEntry( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = SetOfSet (
    @TclArgSt("-comment", "Write a value:"),
    @TclArgSt("-defValue", "")
  );
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );
 
  Text tclCmd = "
  proc ::ExitDialog {} {
   global "+TCLRESULT+"
   set "+TCLRESULT+" [BfoGetText .dial.labent]
   destroy .dial
  }
  global "+TCLRESULT+"
  Dialog .dial -default 0 "+ TclGAKV("-title",aplArg,2)+"
  LabelEntry .dial.labent -text {"+ TclGAV("-defValue",aplArg) +"}
  Label .dial.up -text "+BQt2(TclGAV("-comment",aplArg))+" -justify left
  Button .dial.but -text OK -command [list ::ExitDialog]
  pack .dial.up
  pack .dial.labent
  pack .dial.but
  .dial draw
  return $"+TCLRESULT;

  Set answer = Tcl_Eval(tclCmd);
  answer["result"]
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2(
"Returns a Text variable through asking by a TCL entry. 
The arguments are:
-comment: text to ask for a value. optional.
-title: the window title. optional.
-defValue: default value. optional. ",
"Devuelve una variable de tipo Texto a traves de una pregunta en una ventana
TCL. Los argumentos posibles son:
-comment: comentario para pedir el valor. opcional
-title: titulo de la ventana. opcional
-defValue: valor por defecto. opcional"),
TclDialogEntry);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Date TclDialogDate( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = SetOfSet (
    @TclArgSt("-comment", "Choose a date:")
  );
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );

  Text tclCmd = "
  proc ::ExitDialog {} {
   global "+TCLRESULT+"
   set d [.dial.entryInfo cget -bdate]    
   if {$d eq \"\"} {
     set "+TCLRESULT+" UnknownDate
   } else {
     set "+TCLRESULT+" [DateTol $d]
   }
   destroy .dial
  }

  global "+TCLRESULT+"
  Dialog .dial -default 0 "+ TclGAKV("-title",aplArg,2)+"
  bentrydate .dial.entryInfo
  Label .dial.up -text "+BQt2(TclGAV("-comment",aplArg))+" -justify left
  Button .dial.but -text OK -command [list ::ExitDialog]
  wm protocol .dial WM_DELETE_WINDOW \
	  [list ::ExitDialog]
  pack .dial.up 
  pack .dial.entryInfo
  pack .dial.but 
  .dial draw
  return $"+TCLRESULT;

  Set answer   = Tcl_Eval(tclCmd);
  Text dateTxt = answer["result"];
  If(dateTxt=="", UnknownDate, Eval(dateTxt))
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("Return a Date variable through asking by a TCL entry. 
The arguments are:
-comment: question to ask for the value. optional
-title: the window title.",
"Devuelve una variable de tipo Date a traves de una pregunta en una ventana
 TCL. Los argumentos posibles son:
-comment: comentario para pedir el valor. opcional
-title: titulo de la ventana. opcional"),
TclDialogDate);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclDialogCombo( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set newValArg = Select(valArg, Real (Set reg) {
    Not(reg->Key == "-title")
  });

  Set preValArg = If(TclCheckArg("-title",valArg),
  {
    Text valTitle = TclGAV("-title",valArg);
    newValArg<< SetOfSet(
      @TclArgSt("-titles", TxtListTcl(SetOfText(valTitle))) )
  },
    newValArg
  );
  Set defValArg = preValArg<<SetOfSet(
    @TclArgSt("-cols",    TxtListTcl(SetOfText("col1")) ),
    @TclArgSt("-returns", TxtListTcl(SetOfText("col1")) )
  );
  TclDialogComboPlus(defValArg)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("DEPRECATED. Use TclDialogComboPlus instead. 
Return a Text variable through asking by a TCL entry which
shows a value list to choose. 
The arguments are:
-comment: question. optional
-title: the window title. optional.
-values: list of values to show. required.", 
"DEPRECATED. Use TclDialogComboPlus en su lugar.
Devuelve una variable de tipo Texto a traves de una pregunta en una ventana
TCL que muestra una lista de valores a elegir.
Los argumentos posibles son:
-comment: comentario para pedir el valor. opcional
-title: titulo de la ventana. opcional.
-values: lista de valores. obligatorio"),
TclDialogCombo);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclDialogComboPlus( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = SetOfSet (
    @TclArgSt("-comment", "Choose a value from list:")
  );
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );

  If(TclCheck(SetOfText("-values","-cols","-returns"),aplArg),
  {
    Text tclCmd = "
    proc ::ExitDialog { } {
     global "+TCLRESULT+"
       set "+TCLRESULT+" [.dial.comboInfo  values get "+
           TclGAV("-returns", aplArg)+"]  
     destroy .dial
    }  
    global "+TCLRESULT+"
    set "+TCLRESULT+" {}
    Dialog .dial -default 0 "+TclGAKV("-title",aplArg,2)+"
    bcomboboxplus .dial.comboInfo        \
        -parent .dial \
        -values "+TclGAV("-values", aplArg)+
                 TclGAKV("-cols",         aplArg,0)+
                 TclGAKV("-colkey",       aplArg,0)+
                 TclGAKV("-width",        aplArg,0)+
                 TclGAKV("-widthlb",      aplArg,0)+
                 TclGAKV("-showtitles",   aplArg,0)+
                 TclGAKV("-titles",       aplArg,0)+
                 TclGAKV("-showtitles",   aplArg,0)+
                 TclGAKV("-hiddencols",   aplArg,0)+
                 TclGAKV("-hiddencolslb", aplArg,0)+
                 TclGAKV("-showlabels",   aplArg,0)+
                 TclGAKV("-showlabelkey", aplArg,0)+
                 TclGAKV("-keyhelptext",  aplArg,0)+
                 TclGAKV("-modifycmd",    aplArg,0)+
                 TclGAKV("-postcommand",  aplArg,0)+
                 TclGAKV("-nrows",        aplArg,0)+
                 TclGAKV("-color",        aplArg,0)+"          
    Label .dial.up -text "+
         BQt2(TclGAV("-comment",aplArg))+" -justify left
    Button .dial.but -text OK -command [list ::ExitDialog]
    pack .dial.up 
    wm resizable .dial 1 0
    pack .dial.comboInfo -expand yes -fill x
    pack .dial.but 
    .dial draw
    return $"+TCLRESULT;
    Text TclTrace(tclCmd);
  
    Set answer   = Tcl_Eval(tclCmd);
    Text comboTxt = answer["result"]
  },
  {
    Text TclError(MsgReqArg+":"+NL+" -values"+NL+" -cols"+NL+" -returns");
    "ERROR"
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("Return a Text variable through asking by a TCL combobox 
which shows a value list to choose. 
The arguments are:
-values:   list of values to show. required.
-cols:     list of columns. required.
-returns:  name of column which its value is returned. required.
-colkey:   name of column key of combobox. optional.
-comment:  question. optional
-title:    the window title. optional.
-width:    list with width of each one field. optional.
-widthlb:  width of pop-up listbox. optional.
-titles:   list with titles of each one of labels in combobox. optional.
-showtitles:    false to hidden title of pop-up listbox,true otherways. optional.
-hiddencols:    list of hidden cols in comboboxplus. optional.
-hiddencolslb:  list of hidden cols in pop-up listbox. optional.
-showlabels:    true to show the labels, false otherways. optional.
-showlabelkey:  true to show the label of the key, false otherways. optional.
-keyhelptext:   help text of the key field. optional.
-helptexts:     list with the help texts of all fields. optional.
-modifycmd:     command to execute after selecting an item. optional.
-postcommand:   command to execute before showing the list. optional.
-nrows:  number of visible rows in pop-up listbox. optional.
-color:  color of titles of combobox. optional.", 
"Devuelve una variable de tipo Texto a traves de una pregunta en una ventana
TCL que muestra una lista de valores a elegir.
Los argumentos posibles son:
-values:   lista de valores. obligatorio.
-cols:     lista de columnas. obligatorio.
-returns:  nombre de la columna que se quiere devolver su valor. obligatorio.
-colkey:   nombre de la columna clave del combobox. optional.
-comment:  comentario para pedir el valor. opcional.
-title:    titulo de la ventana. opcional.
-width:    lista con el tama�o de cada uno de los campos. opcional.
-widthlb:  anchura del listbox desplegable. opcional.
-titles:   lista con los titulos de cada una de las etiquetas del combobox.
           opcional.
-helptexts:    lista con los textos de ayuda para cada uno de los campos del
               combobox. opcional.
-hiddencols:   lista de las columnas ocultas del combobox. opcional.
-hiddencolslb: lista de las columnas ocultas del la lista desplegable.
               opcional.
-keyhelptext:  texto de ayuda para el campo clave del combobox. opcional.
-showlabelkey: toma un valor true para mostrar la etiqueta del campo clave
               del combobox. opcional.
-showlabels:   toma un valor true para mostrar las etiquetas del combobox.
               opcional.
-showtitles:   toma un valor false para ocultar el titulo de las columnas.
               opcional.
-modifycmd:    comando Tcl a ejecutar despu�s de seleccionar un elemento.
               opcional.
-postcommand:  comando Tcl a ejecutar antes de mostrar la lista desplegable.
               opcional.
-nrows:  numero de columnas visibles de la lista desplegable. opcional.
-color:  color de las etiquetas del combobox. opcional."),
TclDialogComboPlus);
//////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
Text TclDialogMultiselect( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = Copy(Empty);
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );

  Text tclCmd = "
  proc ::Accept { path } {
   global "+TCLRESULT+"
   set lst [$path.work.bms get selected]  
   set "+TCLRESULT+" $lst 
   destroy $path
  }  

  proc ::Cancel { path } {
   global "+TCLRESULT+"
   set "+TCLRESULT+" \"\"
   destroy $path
  }  

  global "+TCLRESULT+"
  set path [.main.mdi slave "+
      TclGAKV("-title",aplArg,2)+" -modal 1  -resizable 1 -height 400 -width 500]
 
  bmultiselect $path.work.bms "+
              TclGAKV("-unsel",      aplArg,0)+
              TclGAKV("-sel",        aplArg,0)+
              TclGAKV("-labelsel",   aplArg,0)+
              TclGAKV("-labelunsel", aplArg,0)+
              TclGAKV("-font",       aplArg,2)+
              TclGAKV("-onchanged",  aplArg,0)+
              TclGAKV("-unique",     aplArg,0)+
              TclGAKV("-cols",       aplArg,0)+
              TclGAKV("-hiddencols", aplArg,0)+
              TclGAKV("-width",      aplArg,0)+
              TclGAKV("-height",     aplArg,0)+
              TclGAKV("-initialdir", aplArg,0)+"


  grid $path.work.bms   -sticky news

  ButtonBox $path.work.bb -orient horizontal -spacing 10 -padx 0 -pady 0 \
     -homogeneous false
  $path.work.bb add -text [mc \"Accept\"] -relief link -helptext [mc \"Accept\"] \
      -image [::Bitmap::get accept] -compound left -padx 5 -pady 1 \
      -command [list ::Accept $path]
  $path.work.bb add -text [mc \"Cancel\"] -relief link \
    -helptext [mc \"Cancel\"]\
    -image [::Bitmap::get cancelx] -compound left -padx 5 -pady 1 \
    -command [list ::Cancel $path]

  grid $path.work.bms   -sticky news
  grid $path.work.bb    -sticky news

  grid rowconfigure    $path.work 0 -weight 1
  grid columnconfigure $path.work 0 -weight 1
  tkwait window $path
  return $"+TCLRESULT;

  Text TclTrace(tclCmd);
  
  Set answer   = Tcl_Eval(tclCmd);
  answer["result"]
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("Component for creating a multiple selector of items and 
managing its events. A multiple selector consist of two listboxes:
unselected (left) and selected (right) with items which you can move from 
one listbox to the other.

unsel -> unselected values
sel -> selected values
labelsel -> selected values label
labelunsel -> unselected values label
font -> font for the both listboxes
onchanged -> command to be executed each time there is a change in the lists
unique -> only one value can be selected if true
cols -> list with the columns in the listbox
hiddencols -> hidden columns in the listbox
width -> widget width
height -> widget height
initialdir -> default directory when opening a save dialog
"
, 
"Componente para crear un selector multiple y controlar sus eventos.
Consiste en dos listbox:
No seleccionados (izquierda) y seleccionados (derecha) con elementos que
pueden moverse de ona lista a otra.

unsel -> valores no seleccionados
sel -> valores seleccionados
labelsel -> etiqueta para la lista de elementos seleccionados
labelunsel -> etiqueta para la lista de elementos no seleccionados
font -> tipo de fuente para ambos listboxes
onchanged -> comando a ejecutar cada vez que cambia algo en las listas
unique -> si esta a true solamente se permite pasar un elemento a la lista
          de seleccionados
cols -> lista con las columnas de cada ListBox
hiddencols -> aquellas columnas del ListBox que queremos mantener ocultas
width -> ancho total orientativo del widget
height -> alto total orientativo del widget
initialdir -> directorio por defecto cuando se abre una dialogo guardar
"),
TclDialogMultiselect);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Text TclDialogEditSelector( Set valArg )
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = SetOfSet (
    @TclArgSt("-comment", "Choose some values from list:")
  );
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );

  Text If(TclCheck(SetOfText("-list","-cols","-returns"),aplArg),
  {
    Text tclCmd = "
    proc ::ExitDialog { } {
     global "+TCLRESULT+"
     set lst [.dial.bes selection get "+ TclGAV("-returns", aplArg)+"]  
     set "+TCLRESULT+" $lst 
     destroy .dial
    }  
    global "+TCLRESULT+"
    Dialog .dial  -default 0 "+ TclGAKV("-title",aplArg,2)+"
    beditselector .dial.bes       \
        -list "+TclGAV("-list",       aplArg)+
                TclGAKV("-sel",        aplArg,0)+
                TclGAKV("-label",      aplArg,0)+
                TclGAKV("-labelsel",   aplArg,0)+
                TclGAKV("-labelunsel", aplArg,0)+
                TclGAKV("-font",       aplArg,2)+
                TclGAKV("-readonly",   aplArg,0)+
                TclGAKV("-cols",       aplArg,0)+
                TclGAKV("-selcols",    aplArg,0)+
                TclGAKV("-entcols",    aplArg,0)+
                TclGAKV("-hlpcols",    aplArg,0)+
                TclGAKV("-unique",     aplArg,0)+
                TclGAKV("-width",      aplArg,0)+
                TclGAKV("-lwidth",     aplArg,0)+
                TclGAKV("-msgeometry", aplArg,0)+
                TclGAKV("-procedure",  aplArg,0)+"
    
    Label .dial.up -text "+
         BQt2(TclGAV("-comment",aplArg))+" -justify left
    Button .dial.but -text OK -command [list ::ExitDialog ]
    pack .dial.up 
    pack .dial.bes -expand yes -fill x
    pack .dial.but 
    .dial draw
    return $"+TCLRESULT;

    Text TclTrace(tclCmd);
  
    Set answer   = Tcl_Eval(tclCmd);
    answer["result"]
  },
  {
    Text TclError(MsgReqArg+":"+NL+" -list"+NL+" -cols"+NL+" -returns");
    "ERROR"
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(I2("Return a Text variable through asking by a TCL edit
selector which shows a value list to choose some values from it. 
The arguments are:
-list:     list of values to show. required.
-cols:     list of columns. required.
-returns:  name of columns whose values are returned. required.

-sel:      list of values that are selected. optional
-title:    the window title. optional.
-label
-labelsel
-labelunsel
-font
-readonly
-cols
-selcols
-entcols
-hlpcols
-unique
-width
-lwidth
-msgeometry
-procedure", 
"Devuelve una variable de tipo Texto a traves de una pregunta en una ventana
TCL que muestra una lista de valores permitiendo elegir varios de ellos.
Los argumentos posibles son:
-list:     list of values to show. required.
-cols:     list of columns. required.
-returns:  name of columns whose values are returned. required.

-sel:      list of values that are selected. optional
-title:    the window title. optional.
-label
-labelsel
-labelunsel
-font
-readonly
-cols
-selcols
-entcols
-hlpcols
-unique
-width
-lwidth
-msgeometry
-procedure"),
TclDialogEditSelector);
//////////////////////////////////////////////////////////////////////////////
